import { useState, useEffect } from "react";

export default function SchoolFinder() {
  const [schools, setSchools] = useState([]);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (search.length > 0) {
      setLoading(true);
      fetch(`/api/schools?search=${search}`)
        .then((res) => res.json())
        .then((data) => {
          setSchools(data);
          setLoading(false);
        });
    }
  }, [search]);

  return (
    <div>
      <h1>School Finder</h1>
      <input
        type="text"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
      {loading && <p>Loading...</p>}
      <ul>
        {schools.map((school) => (
          <li key={school.id}>{school.name}</li>
        ))}
      </ul>
    </div>
  );
}
